package look.word.datasource.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

/**
 * 数据源配置
 *
 * @author jiejie
 * @date 2022/10/10
 */
@Configuration
@PropertySource("classpath:application.yaml")
public class DataSourceConfig {

    @Bean(name = "mysqlDataSource1")
    @ConfigurationProperties(prefix = "spring.datasource.mysql-datasource1")
    public DataSource dataSource1() {
        DruidDataSource build = DruidDataSourceBuilder.create().build();
        return build;
    }


    @Bean(name = "mysqlDataSource2")
    @ConfigurationProperties(prefix = "spring.datasource.mysql-datasource2")
    public DataSource dataSource2() {
        DruidDataSource build = DruidDataSourceBuilder.create().build();
        return build;
    }

    /**
     * 配置数据源事务
     */
    @Bean
    public PlatformTransactionManager transactionManager(DataSource mysqlDataSource2) {
        return new DataSourceTransactionManager(mysqlDataSource2);
    }
}
