package look.word.datasource.config;

/**
 * 数据源枚举
 *
 * @author jiejie
 * @date 2022/10/11
 */
public enum DataSourceType {

    MYSQL_DATASOURCE1,

    MYSQL_DATASOURCE2,

}
