package look.word.datasource.mapper;

import look.word.datasource.entity.Book;
import org.apache.ibatis.annotations.Param;

/**
 * 书映射器
 *
 * @author jiejie
 * @date 2022/10/10
 */
public interface BookMapper {

    Book getById(@Param("id") int id);

    int updatePrice(@Param("id") int id, @Param("price") double price);

}
