package look.word.datasource;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author : look-word
 * 2022-10-10 22:41
 * @EnableTransactionManagement 开启事务管理器
 * @EnableAspectJAutoProxy 开启Spring Boot对AOP的支持
 **/
@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableTransactionManagement
@EnableAspectJAutoProxy
@MapperScan("look.word.datasource.mapper")
public class MultipleDataSourcesApp {
    public static void main(String[] args) {
        SpringApplication.run(MultipleDataSourcesApp.class, args);
        log.info("启动成功！！！！");
    }
}
