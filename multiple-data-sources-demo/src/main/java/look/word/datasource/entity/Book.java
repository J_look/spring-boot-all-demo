package look.word.datasource.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * book
 *
 * @author
 */
@Data
public class Book implements Serializable {
    private Integer id;

    /**
     * 作者信息
     */
    private String author;

    /**
     * 书籍名称
     */
    private String name;

    /**
     * 价格
     */
    private Long price;

    /**
     * 上架时间
     */
    private Date createTime;

    /**
     * 书籍描述
     */
    private String description;

    private static final long serialVersionUID = 1L;
}
