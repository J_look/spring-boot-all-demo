package look.word.datasource.service.impl;

import look.word.datasource.aop.datasource.TargetDataSource;
import look.word.datasource.config.DataSourceType;
import look.word.datasource.entity.Book;
import look.word.datasource.mapper.BookMapper;
import look.word.datasource.service.BookService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author : look-word
 * 2022-10-10 23:04
 **/
@Service
public class BookServiceImpl implements BookService {

    @Resource
    private BookMapper bookMapper;

    @Override
    public Book getById(int id) {
        return bookMapper.getById(id);
    }

    @Transactional(rollbackFor = RuntimeException.class)
    @Override
    @TargetDataSource(DataSourceType.MYSQL_DATASOURCE2)
    public int updatePrice(int id, double price) {
        int result = 0;
        if (bookMapper.getById(id) != null) {
            result = bookMapper.updatePrice(id, price);
            throw new RuntimeException("111111");
        }
        return result;

    }
}
