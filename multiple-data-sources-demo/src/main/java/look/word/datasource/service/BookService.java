package look.word.datasource.service;

import look.word.datasource.entity.Book;

import java.math.BigDecimal;

/**
 * 书服务
 *
 * @author jiejie
 * @date 2022/10/10
 */
public interface BookService {

    Book getById(int id);

    int updatePrice(int id, double price);
}
