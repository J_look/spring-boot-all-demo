package look.word.datasource.aop.datasource;

import look.word.datasource.config.DataSourceType;

import java.lang.annotation.*;

@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TargetDataSource {

    DataSourceType value() default DataSourceType.MYSQL_DATASOURCE1;
}
