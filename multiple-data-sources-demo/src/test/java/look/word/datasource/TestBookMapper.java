package look.word.datasource;

import look.word.datasource.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * @author : look-word
 * 2022-10-10 23:11
 **/
@SpringBootTest
public class TestBookMapper {
    @Resource
    private BookService bookService;

    @Test
    void testGetOne() {
        System.out.println(bookService.getById(1));
    }

    @Test
    void updatePrice() {
        System.out.println(bookService.updatePrice(1, 777));
    }
}
