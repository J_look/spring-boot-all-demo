package com.nowcoder.community.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Data
public class User implements UserDetails {

    private int id;
    private String username;
    private String password;
    private String salt;
    private String email;
    private int type;
    private int status;
    private String activationCode;
    private String headerUrl;
    private Date createTime;

    //权限
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> permissions = new ArrayList<>();
        permissions.add((GrantedAuthority) () -> {
            switch (type) {
                case 1:
                    return "ADMIN";
                default:
                    return "USER";
            }
        });
        return permissions;
    }

    // true 帐户未过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // true 帐户未锁定
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // true 凭证未过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // true 账号是否可用
    @Override
    public boolean isEnabled() {
        return true;
    }
}
